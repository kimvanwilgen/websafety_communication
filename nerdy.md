**Subject:** Safety first: take action to fix vulnerability at {{Company}}



Hi, `{{first_name}}`!

I’m a security researcher from the Netherlands and my goal is to increase security awareness. My main mission is to make our digital world safer by following responsible disclosure principles.

Long story short: I found the security issue on the `{{Company}}` website.

There is a public folder with unrestricted access, and this type of vulnerability usually can be used to compromise the website: data breaches, money loss, business damage.


If you don't trust me, that’s totally understandable, you don’t have to.

You may think this email looks like spam/scum/fishing.
To be honest, I don't care what do you think of me or this message.

But.

There is a vulnerability, and it has to be fixed.

Find some techie you trust and ask them to check and fix or contact me for the details.


**------------------------------------------------------------------**

Best,

Max 

Security Researcher at WebSafety.Ninja

Whatsapp/Telegram +31 617193900

Dutch Chamber of Commerce number (KVK) 72801980